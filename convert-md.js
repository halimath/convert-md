#!/usr/bin/env node
"use strict"

const path = require("path")
const cli = require(path.resolve(__dirname, "lib", "cli"))

cli(process.argv)
