"use strict"

const convertMd = require("./lib/main")

convertMd("# Hello world\nof _markdown_", {
  type: "html"
})
  .then(stream => {
    stream.pipe(process.stdout)
  })
  .catch(err => {
    console.error(err)
  })
