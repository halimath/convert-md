# convert-md

`convert-md` is a tool that converts
[markdown](https://daringfireball.net/projects/markdown/) written files to
either
* HTML
* PDF
* PNG

`convert-md` can either be used as a command line tool.

## Installation

`convert-md` can be installed using [npm](http://npmjs.org/).

```
$ npm install convert-md
```

If you plan to use `convert-md` as a stand alone tool from the command line, it
is recommended to install it globally:

```
$ npm install -g convert-md
```

## Usage

### As a tool from the command line

```
$ convert-md file.md
```

will create a file `file.md.html` in your current directory. You can specify
the type of file using the `-t` option. You can also provide a custom output
file name using the `-o` option:

```
$ conver-md -t pdf -o file.pdf file.md
```

### As a Javascript library from node applications

Import the conversion function:

```javascript
const convertMd = require("convert-md")
```

`convertMd` is a function that accepts either one, two or three arguments:

```javascript
convertMd (markdownString, options, callback)
```

* `markdownString` - a string containing the markdown formatted input string
* `options` - an optional object that may contain a key `type` describing the
  output type:
  * `html`
  * `pdf`
  * `png`
* `callback` - an optional function that receives two arguments:
  * `err` - an error object in case something went wrong
  * `output` - a `ReadableStream` that containing the generated output

If no callback is given the function returns a EcmaScript 6 `Promise` object
that resolves to the generated content.

The following snipped demonstrates the usage:

```javascript
"use strict"

const convertMd = require("convert-md")

convertMd("# Hello world\nof _markdown_", {
  type: "html"
})
  .then(stream => {
    stream.pipe(process.stdout)
  })
  .catch(err => {
    console.error(err)
  })
```
