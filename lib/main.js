"use strict"

const path = require("path")

const MarkdownContent = require("./MarkdownContent")
const RenderedContent = require("./RenderedContent")

module.exports = function convertMd (markdownString, options, callback) {
  if (typeof(options) === "function") {
    callback = options
    options = {}
  }

  options = options || {
  }

  options.type = options.type || "pdf"

  let promise = new MarkdownContent(markdownString)
    .toHtml()
    .then(htmlContent => {
      return htmlContent.applyTemplate(path.resolve(__dirname, "template.html"))
    })
    .then(htmlContent => {
      if (options.type === "html") {
        return htmlContent
      }

      if (["pdf", "png"].indexOf(options.type) >= 0) {
        return RenderedContent.fromHtml(htmlContent, options.type)
      }
    })

  if (typeof(callback) !== "function") {
    return promise
  }

  promise
    .then(renderedContent => {
      callback(null, renderedContent)
    })
    .catch(callback)
}
