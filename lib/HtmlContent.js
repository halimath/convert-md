"use strict"

const fs = require("fs")

const WritableContent = require("./WritableContent")

class HtmlContent extends WritableContent {
  constructor (content) {
    super(content)
  }

  applyTemplate (template) {
    return new Promise((resolve, reject) => {
      fs.readFile(template, "utf8", (err, templateContent) => {
        if (err) {
          return reject(err)
        }

        resolve(new HtmlContent(templateContent.replace("{{{content}}}", this.content)))
      })
    })
  }
}

module.exports = HtmlContent
