"use strict"

const showdown = require("showdown")

const HtmlContent = require("./HtmlContent")

function markdownExtensions () {
  return [
    {
      type: "lang",
      regex: /^a(uthor)?:\s*(.+?)\s*$/gm,
      replace: "<div class=\"author\">$2</div>"
    },
    {
      type: "lang",
      regex: /^rev(ision)?:\s*(.+?)\s*$/gm,
      replace: "<div class=\"revision\">$2</div>"
    },
    {
      type: "output",
      regex: /<\/?t((head)|(body))>/g,
      replace: ""
    }
  ]
}


class MarkdownContent {
  constructor (content) {
    this._content = content
  }

  toHtml () {
    return new Promise((resolve, reject) => {
      let converter = new showdown.Converter({
        extensions: [markdownExtensions]
      })
      converter.setOption("tables", true)
      resolve(new HtmlContent(converter.makeHtml(this._content)))
    })
  }
}

module.exports = MarkdownContent
