"use strict"

var fs = require("fs")
var system = require("system")
var webpage = require("webpage")

var page = webpage.create()

page.viewportSize = {
  width: 2000,
  height: 2000
}

page.paperSize = {
  format: "A4",
  orientation: "portrait",
  margin: {
    top: "2cm",
    left: "2cm",
    right: "2cm",
    bottom: "2cm"
  }
}

page.content = fs.read("/dev/stdin")

page.render("/dev/stdout", {
  format: system.args[1] || "pdf"
})
phantom.exit()
