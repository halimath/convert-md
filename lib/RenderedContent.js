"use strict"

const path = require("path")
const childProcess = require('child_process')
const phantomjs = require("phantomjs-prebuilt")

const WritableContent = require("./WritableContent")

class RenderedContent extends WritableContent {
  static fromHtml (htmlContent, type) {
    type = type || "pdf"

    return new Promise((resolve, reject) => {
      let process = childProcess.exec(`${phantomjs.path} ${path.resolve(__dirname, "phantomjs", "render.js")} ${type}`, {
        encoding: "buffer",
        maxBuffer: 2048 * 2048
      }, (err, stdout, stderr) => {
        if (err) {
          return reject(err)
        }

        resolve(new RenderedContent(stdout))
      })

      process.stdin.write(htmlContent.content)
      process.stdin.end()
    })
  }

  constructor (content) {
    super(content)
  }
}

module.exports = RenderedContent
