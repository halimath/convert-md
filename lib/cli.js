"use strict"

const fs = require("fs")

const commander = require("commander")

const convertMd = require("./main")

let Version = require("../package.json").version

module.exports = function cli (args) {
  args = args || process.argv

  commander
    .version(`convert-md ${Version}`)
    .description('Convert markdown files to HTML, PDF or PNG.')
    .usage("[options] <markdown file>")

    .option("-t, --type [type]", "Output type; one of html, pdf, png; html is the default", "html")
    .option("-o, --out [file]", "Output file name; defaults to input file name with replaced extension")

    .parse(args)

  if (commander.args.length < 1) {
    console.error(`${args.slice(0, 2).join(" ")}: Missing input file.`)
    commander.outputHelp()
    process.exit(1)
  }

  let outFilename = commander.out || `${commander.args[0]}.${commander.type}`

  new Promise((resolve, reject) => {
      fs.readFile(commander.args[0], "utf8", (err, content) => {
        if (err) {
          return reject(err)
        }

        resolve(content)
      })
    })
    .then(markdownString => {
      return convertMd(markdownString, {
        type: commander.type
      })
    })
    .then(renderedContent => {
      return renderedContent.writeToFile(outFilename)
    })
    .then(() => {
      console.log(outFilename)
    })
    .catch(err => {
      console.error(err)
      console.error(err.stack)
      process.exit(2)
    })
}
