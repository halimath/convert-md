"use strict"

const fs = require("fs")
const stream = require("stream")

class WritableContent extends stream.Readable {
  constructor (content, options) {
    super(options)
    this._content = content
  }

  get content () {
    return this._content
  }

  writeToFile (filename) {
    return new Promise((resolve, reject) => {
      let writableStream = fs.createWriteStream(filename)
        .on("close", resolve)
        .on("error", reject)

      this.pipe(writableStream)
    })
  }

  _read (size) {
    this.push(this.content)
    this.push(null)
  }
}

module.exports = WritableContent
